---
layout: home
lang: en
---

# Roar~&#x1F3F3;&#xFE0F;&#x200D;&#x1F308;&#65039;

This's a thing for giving me nice checkmarks at places :3

- <a href="https://keybase.io/leon4" rel="me">leon4@keybase.io</a>
- <s><a href="https://keyoxide.org/hkp/713a02a7252c18bef9fa6497f6daadef81c456ff" rel="me">Keyoxide OpenPGP</a></s> <a href="https://keyoxide.org/aspe:keyoxide.org:JLDK6OZD5J3PVKY66RKUKED6MM" rel="me">Keyoxide ASP</a>

## &#x1F3F3;&#xFE0F;&#x200D;&#x1F308;&#128062;&#65039;

- PGP: <a class="theboot" href="https://keybase.io/leon4/pgp_keys.asc?fingerprint=713a02a7252c18bef9fa6497f6daadef81c456ff">713A 02A7 252C 18BE F9FA 6497 F6DA ADEF 81C4 56FF</a>
- SSH: PGP &#10133;&#65038; <a class="theboot" href="https://gitlab.com/Leonard0.keys">SHA256:na3jkxAg5FD5vit09gOvifcpnJWfZkSGaNvowleogKw</a>
